#include "KeyboardInput.h"

const unique_ptr<Keyboard> KeyboardInput::KEYBOARD(new Keyboard);
const unique_ptr<Keyboard::KeyboardStateTracker> KeyboardInput::TRACKER(new Keyboard::KeyboardStateTracker);

void KeyboardInput::initialise() {
  if (keyBindings.find(MOVE_UP) == keyBindings.end()) {
    keyBindings.insert(pair<Event, vector<int>>(MOVE_UP, vector<int>()));
  }
  if (keyBindings.find(MOVE_DOWN) == keyBindings.end()) {
    keyBindings.insert(pair<Event, vector<int>>(MOVE_DOWN, vector<int>()));
  }
  if (keyBindings.find(MOVE_LEFT) == keyBindings.end()) {
    keyBindings.insert(pair<Event, vector<int>>(MOVE_LEFT, vector<int>()));
  }
  if (keyBindings.find(MOVE_RIGHT) == keyBindings.end()) {
    keyBindings.insert(pair<Event, vector<int>>(MOVE_RIGHT, vector<int>()));
  }

  keyBindings[MOVE_UP].push_back(KEYBOARD.get()->Up);
  keyBindings[MOVE_UP].push_back(KEYBOARD.get()->W);
  keyBindings[MOVE_DOWN].push_back(KEYBOARD.get()->Down);
  keyBindings[MOVE_DOWN].push_back(KEYBOARD.get()->S);
  keyBindings[MOVE_LEFT].push_back(KEYBOARD.get()->Left);
  keyBindings[MOVE_LEFT].push_back(KEYBOARD.get()->A);
  keyBindings[MOVE_RIGHT].push_back(KEYBOARD.get()->Right);
  keyBindings[MOVE_RIGHT].push_back(KEYBOARD.get()->D);
}

Event KeyboardInput::checkForInput() const {
  Keyboard* keyboard = KEYBOARD.get();
  auto state = keyboard->GetState();
  Keyboard::KeyboardStateTracker* tracker = TRACKER.get();
  tracker->Update(state);

  for (auto kvp : keyBindings) {
    for (auto key : kvp.second) {
      if (tracker->IsKeyPressed((Keyboard::Keys) key)) {
        return kvp.first;
      }
    }
  }
}