#include "TextureManager.h"

map<string, ID3D11ShaderResourceView*> TextureManager::textures;

void TextureManager::cleanup() {
  for (auto kvp : textures) {
    if (kvp.second) {
      kvp.second->Release();
    }
  }
}

HRESULT TextureManager::loadTexture(ID3D11Device* device, string identifier, const wchar_t fileName[]) {
  ID3D11ShaderResourceView* texture;
  HRESULT hr = CreateDDSTextureFromFile(device, fileName, nullptr, &texture);
  if (FAILED(hr)) {
    return hr;
  }
  textures.insert(pair<string, ID3D11ShaderResourceView*> (identifier, texture));
  return S_OK;
}
