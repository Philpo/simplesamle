#include "Input.h"

map<Event, vector<int>> Input::keyBindings;

bool Input::contains(const vector<int>& vector, const int toCheck) const {
  for (auto value : vector) {
    if (value == toCheck) {
      return true;
    }
  }
  return false;
}