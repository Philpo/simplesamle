#include "Sprite.h"

void Sprite::setPosition(const float x, const float y) {
  this->x = x;
  this->y = y;
}

void Sprite::update(DWORD* elapsedTime, Event keyPressedEvent) {}

void Sprite::draw(DWORD* elapsedTime, SpriteBatch* spriteBatch) const {
  spriteBatch->Draw(&texture, XMFLOAT2(x, y), nullptr, Colors::White);
}
