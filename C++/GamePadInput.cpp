#include "GamePadInput.h"

const unique_ptr<GamePad> GamePadInput::GAME_PAD(new GamePad);
const unique_ptr<GamePad::ButtonStateTracker> GamePadInput::TRACKER(new GamePad::ButtonStateTracker);

void GamePadInput::initialise() {
  if (keyBindings.find(MOVE_UP) == keyBindings.end()) {
    keyBindings.insert(pair<Event, vector<int>>(MOVE_UP, vector<int>()));
  }
  if (keyBindings.find(MOVE_DOWN) == keyBindings.end()) {
    keyBindings.insert(pair<Event, vector<int>>(MOVE_DOWN, vector<int>()));
  }
  if (keyBindings.find(MOVE_LEFT) == keyBindings.end()) {
    keyBindings.insert(pair<Event, vector<int>>(MOVE_LEFT, vector<int>()));
  }
  if (keyBindings.find(MOVE_RIGHT) == keyBindings.end()) {
    keyBindings.insert(pair<Event, vector<int>>(MOVE_RIGHT, vector<int>()));
  }

  keyBindings[MOVE_UP].push_back(D_PAD_UP);
  keyBindings[MOVE_DOWN].push_back(D_PAD_DOWN);
  keyBindings[MOVE_LEFT].push_back(D_PAD_LEFT);
  keyBindings[MOVE_RIGHT].push_back(D_PAD_RIGHT);

  // previous to adding an enum to identify gamepad buttons, the below was tried
  // this didn't work since ButtonStateTracker.dpadUp etc. are ButtonState enums, which default to UP (0)
  // so every event maps to 0, rather than an identifier for the specific button
  // see KeyboardInput.cpp for the ideal way of doing this
  keyBindings[MOVE_UP].push_back(TRACKER.get()->dpadUp);
  keyBindings[MOVE_DOWN].push_back(TRACKER.get()->dpadDown);
  keyBindings[MOVE_LEFT].push_back(TRACKER.get()->dpadLeft);
  keyBindings[MOVE_RIGHT].push_back(TRACKER.get()->dpadRight);
}

Event GamePadInput::checkForInput() const {
  GamePad* gamePad = GAME_PAD.get();
  auto state = gamePad->GetState(0);

  if (state.IsConnected()) {
    GamePad::ButtonStateTracker* tracker = TRACKER.get();
    tracker->Update(state);

    for (auto kvp : keyBindings) {
      if (contains(kvp.second, D_PAD_UP)) {
        if (tracker->dpadUp == GamePad::ButtonStateTracker::HELD || tracker->dpadUp == GamePad::ButtonStateTracker::PRESSED) {
          return kvp.first;
        }
      }
      if (contains(kvp.second, D_PAD_DOWN)) {
        if (tracker->dpadDown == GamePad::ButtonStateTracker::HELD || tracker->dpadDown == GamePad::ButtonStateTracker::PRESSED) {
          return kvp.first;
        }
      }
      if (contains(kvp.second, D_PAD_LEFT)) {
        if (tracker->dpadLeft == GamePad::ButtonStateTracker::HELD || tracker->dpadLeft == GamePad::ButtonStateTracker::PRESSED) {
          return kvp.first;
        }
      }
      if (contains(kvp.second, D_PAD_RIGHT)) {
        if (tracker->dpadRight == GamePad::ButtonStateTracker::HELD || tracker->dpadRight == GamePad::ButtonStateTracker::PRESSED) {
          return kvp.first;
        }
      }
    }
  }
}