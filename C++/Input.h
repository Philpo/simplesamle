#pragma once
#include <map>
#include <vector>

using namespace std;

enum Event { NONE = 0, MOVE_UP = 1, MOVE_DOWN = 2, MOVE_LEFT = 3, MOVE_RIGHT = 4 };

class Input {
public:
  Input() {}
  ~Input() {}
  virtual void initialise() = 0;
  virtual Event checkForInput() const = 0;
protected:
  static map<Event, vector<int>> keyBindings;
  bool contains(const vector<int>& vector, const int toCheck) const;
};

