#pragma once
#include "Input.h"
// have to include Windows here to prevent a build error in Keyboard.h
// build error: C2601 syntax error identifier UINT (Keyboard.h line 463)
#include <Windows.h>
#include "DirectXTK\Inc\Keyboard.h"
using namespace DirectX;

class KeyboardInput : public Input {
public:
  KeyboardInput() {}
  ~KeyboardInput() {}
  void initialise() override;
  Event checkForInput() const override;
private:
  static const unique_ptr<Keyboard> KEYBOARD;
  static const unique_ptr<Keyboard::KeyboardStateTracker> TRACKER;
};

