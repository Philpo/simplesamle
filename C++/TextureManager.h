#pragma once
#include <map>
#include <string>
#include <d3d11.h>
#include "DDSTextureLoader.h"

using namespace DirectX;
using namespace std;

class TextureManager {
public:
  static void cleanup();
  static HRESULT loadTexture(ID3D11Device* device, string identifier, const wchar_t fileName[]);
  inline static ID3D11ShaderResourceView& getTexture(string identifier) { return *textures.at(identifier); }
  inline static int getNumTextures() { return textures.size(); }
private:
  static map<string, ID3D11ShaderResourceView*> textures;
};

