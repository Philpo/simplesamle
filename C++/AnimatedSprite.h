#pragma once
#include "Sprite.h"
class AnimatedSprite : public Sprite {
public:
  AnimatedSprite(float xPos, float yPos, ID3D11ShaderResourceView& texture, DWORD frameTime, int frameHeight, int frameWidth, int numFrames);
  ~AnimatedSprite();

  void update(DWORD* elapsedTime, Event keyPressedEvent) override;
  void draw(DWORD* elapsedTime, SpriteBatch* spriteBatch) const override;
private:
  DWORD timeSinceLastFrame, frameTime;
  RECT* currentFrame;
  int frameHeight, frameWidth, frameCount, numFrames, facingDirection;
  const float moveSpeed;
};

