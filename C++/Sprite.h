#pragma once
#include <d3d11.h>
#include "SpriteBatch.h"
#include "Input.h"

using namespace DirectX;

class Sprite {
public:
  Sprite(float x, float y, ID3D11ShaderResourceView& texture) : x(x), y(y), texture(texture) {}
  virtual ~Sprite() {}

  inline float getX() const { return x; }
  inline float getY() const { return y; }

  void setPosition(const float x, const float y);
  virtual void update(DWORD* elapsedTime, Event keyPressedEvent);
  virtual void draw(DWORD* elapsedTime, SpriteBatch* spriteBatch) const;
protected:
  float x, y;
  ID3D11ShaderResourceView& texture;
};

