#include "AnimatedSprite.h"

AnimatedSprite::AnimatedSprite(float xPos, float yPos, ID3D11ShaderResourceView& texture, DWORD frameTime, int frameHeight, int frameWidth, int numFrames) : Sprite(xPos, yPos, texture)
, frameTime(frameTime), frameHeight(frameHeight), frameWidth(frameWidth), numFrames(numFrames), timeSinceLastFrame(0), frameCount(0), moveSpeed(2.5f)
, facingDirection(0) {
  currentFrame = new RECT();
  currentFrame->top = currentFrame->left = 0;
  currentFrame->bottom = frameHeight;
  currentFrame->right = frameWidth;
}

AnimatedSprite::~AnimatedSprite() {
  delete currentFrame;
}

void AnimatedSprite::update(DWORD* elapsedTime, Event keyPressedEvent) {
  timeSinceLastFrame += *elapsedTime;

  switch (keyPressedEvent) {
    case MOVE_UP:
      y -= moveSpeed;
      if (facingDirection != 0) {
        frameCount = 0;
      }
      facingDirection = 0;
      break;
    case MOVE_LEFT:
      x -= moveSpeed;
      if (facingDirection != 1) {
        frameCount = 0;
      }
      facingDirection = 1;
      break;
    case MOVE_DOWN:
      y += moveSpeed;
      if (facingDirection != 2) {
        frameCount = 0;
      }
      facingDirection = 2;
      break;
    case MOVE_RIGHT:
      x += moveSpeed;
      if (facingDirection != 3) {
        frameCount = 0;
      }
      facingDirection = 3;
      break;
    default:
      break;
  }

  if (timeSinceLastFrame >= frameTime) {
    timeSinceLastFrame = 0;

    currentFrame->left = frameWidth * frameCount;
    currentFrame->right = frameWidth + frameWidth * frameCount;
    currentFrame->top = facingDirection * frameHeight;
    currentFrame->bottom = facingDirection * frameHeight + frameHeight;

    frameCount++;

    if (frameCount > (numFrames - 1)) {
      frameCount = 0;
    }
  }
}

void AnimatedSprite::draw(DWORD* elapsedTime, SpriteBatch* spriteBatch) const {
  spriteBatch->Draw(&texture, XMFLOAT2(x, y), currentFrame, Colors::White);
}