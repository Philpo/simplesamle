#pragma once
#include "Input.h"
#include "DirectXTK\Inc\GamePad.h"
using namespace DirectX;

// added enum to identify each gamepad button, since GamePad.h doesn't provide one
enum GamePadButton {
  A = 0
, B = 1
, X = 2
, Y = 3
, D_PAD_UP = 4
, D_PAD_DOWN = 5
, D_PAD_LEFT = 6
, D_PAD_RIGHT = 7
, LEFT_STICK = 8
, LEFT_TRIGGER = 9
, LEFT_BUMPER = 10
, RIGHT_STICK = 11
, RIGHT_TRIGGER = 12
, RIGHT_BUMPER = 13
, START = 14
, SELECT = 15
};

class GamePadInput : public Input {
public:
  GamePadInput() {}
  ~GamePadInput() {}
  void initialise() override;
  Event checkForInput() const override;
private:
  static const unique_ptr<GamePad> GAME_PAD;
  static const unique_ptr<GamePad::ButtonStateTracker> TRACKER;
};

